import os
import openai
import argparse
import json
from dotenv import dotenv_values
from langchain.document_loaders import PyPDFDirectoryLoader
from langchain.chat_models import AzureChatOpenAI
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from langchain.chains import SequentialChain
from langchain.prompts import ChatPromptTemplate
#from validation import validate
class ExclusionAnalyzer:
    def __init__(self, input_dir, output_file):
        self.input_dir = input_dir
        self.output_file = output_file
        self._load_environment_variables()

    def _load_documents(self):
        loader = PyPDFDirectoryLoader(self.input_dir)
        return loader.load()

    
    def _initialize_exclusion_chain(self, template):

        llm = AzureChatOpenAI(
            model_name=os.environ["OPENAI_MODEL_COMPLETION"],
            deployment_name=os.environ["OPENAI_MODEL_COMPLETION"],
            temperature=0,
        )
        prompt_template = PromptTemplate(
            input_variables=["sp_text"], template=template
        )

        exclusion_chain = LLMChain(
            llm=llm, prompt=prompt_template, output_key="Exclusion"
        )

        return exclusion_chain

    def _load_environment_variables(self):
        env_name = "tcw.env"  # change to your own .env file name
        config = dotenv_values(env_name)
        for k, v in config.items():
            os.environ[k] = v
    def analyze(self, start_page=None, end_page=None, template=""):
        documents = self._load_documents()
        exclusion_chain = self._initialize_exclusion_chain(template)

        query = ""
        exclusions = []
        

        start_page = start_page or 0
        end_page = end_page or len(documents)
        #print(start_page)
        for i, doc in enumerate(documents[start_page:end_page], start=start_page):
            query += doc.page_content
            print("Page Contains:", page_content, procedure)
            #print(f"Processing page {i}")
        result = exclusion_chain(query)
        exclusion_text = result["Exclusion"]
        #print(exclusion_text)
        exclusions.append(exclusion_text)

        self._save_results(exclusions)

    def analyze_v1(self, start_page=None, end_page=None, template="", chunk_len_pages = 200, chunk_overlap_pages = 2):
        '''
        Analyze Transcript and compute Trustworthiness. 
        '''
        # loads pages in the docs 
        documents = self._load_documents()
        print('Number of Pages:', len(documents))
        exclusion_chain = self._initialize_exclusion_chain(template)
        output = ""
        overlap = chunk_overlap_pages
        #print('Overlap:',overlap)
        chunk_len = chunk_len_pages
        pointer = start_page or 0
        end_page = end_page or len(documents)
        while pointer<end_page:
            p_start = pointer
            query = ""
            for j in range(0, chunk_len):
                if p_start+j<end_page:
                    query += documents[p_start+j].page_content
                    #print(f"Processing file: {documents[p_start+j].file_path}")  # Add this line to display the filename
                    pointer = p_start+j+10

            chunk_result = exclusion_chain(query)
            print(f"Processing page {p_start} to {pointer}")
            output = chunk_result["Exclusion"]
            print(output)
            self._save_results("{"+output+"}")

    def _save_results(self, exclusions):
        with open(self.output_file, "a") as file:
            json_inp = json.dumps(exclusions, indent=2)
            #print(json_inp)
            file.write(json_inp)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Analyze exclusions from PDF documents")
    parser = argparse.ArgumentParser(description="Analyze exclusions from PDF documents")
    parser.add_argument("-i", "--input_dir", required=True, help="Input directory containing PDF files")
    parser.add_argument("-o", "--output_file", required=True, help="Name of the JSON file to save results")
    parser.add_argument("-s", "--start_page", type=int, help="Start page for analysis")
    parser.add_argument("-e", "--end_page", type=int, help="End page for analysis")
    args = parser.parse_args()
    args = parser.parse_args()
    template = """
        You are an expert information analyst and an AI assistant. 

        You are given a transcript that describes a financial product meeting that speaker addressing advisor, client community. You task is to identify the "Trustworthiness", that is calucated
        on 4 factors. These are Credibility, Reliability, Intimacy and Self-orientation. Trustworthiness = (Credibility, Reliability, Intimacy) / Self-orientation


        It takes inputs from 0 to infinity, which is the results you can get from your trustworthiness equation, and outputs a number between 0 and 1. We can adjust the "shape" of the graph by changing the coefficient of x. Here's a table of the normalized values for some scores based on the functions (1) y = 1-e^(-.1x) and (2) y = 1-e^(-.05x). You can see that it affects how "hard" it is to get a certain score.
 
        Compute Trustworthiness and normalize Trustworthiness using the function => y = 1-e^(-x) and the value
        should be between 0 and 1

        Credibility	Attributes as follows:				
        1. FIller words: How many filler words were used in the answers? Examples. poor dictation (um, uh, you know, so, like)
        2. Passion: How passionate was the product expert in their answer? Examples. "passionate" "enjoy" "I care" "exciting" "game changing" "groundbreaking"

        Reliability	Attributes as follows:			
        1. Jargon: How much jargon was used in the answers? Examples: acronymns, specialised word
        2. Forward statements: How many forward looking statements does the product expert make? Examples. "outlook", "future", "coming months", expect, estimate, next[period]
        3. Accountability: Of those forward looking statements how many were addressed later in the meeting? Example. If the subject associated with the forward looking statement is discussed later in the meeting
        4. Accountability: Of those forward looking statements how many were addressed in the next meeting? Example. If the subject associated with the forward looking statement is discussed in the next meeting(s)
        5. Speech consistency:	What is the consistency of speech? Example. The product expert shifting opinion on the agenda item without reasoning

        Intimacy Attributes as follows:
        1. Human connection: Is there a conversation that is off topic and human. Example. Keywords: family, friends, pets, sport, travel, hobby
        2. Naming:Does the product expert name attendees when addressing their question. Example. First name of the person asking the question
        3. Vulnerability:Does the product expert use the words "I don't know" or "my failure" or "my mistake". Example. Keywords: "I dont know", "my failure", "my mistake", "sorry", "apologies"


        Self-Orientation Attributes as follows:
        1. Blame:Does the product expert blame others? Example.Look for phrases that blame other people
        2. Name-drop: Does the product expert name-drop through the meeting? Example. Look for names other than the first name of the person asking the question
        3. Curiosity: How frequently does the product expert answer a question with another question? Example. Look for times where the product experts ask attendees questions
        4. Interupt: How frequently did the product expert interupt the questionnaire. Example. Look for times where the product expert does not allow the question to be finished

        Please generate resonse format it nicely as follows:
		
		1. Summarize the Transcript
		2. Summarize 
            - Overall Sentiment
            - Subjectivity
            - Objectivity
            - Polarity

        3. Compute the Trustworthiness based the equation provided. When justification is given, give as much as posiible context.

        4. Justification reasoning for each of the component:Evaluated Score and Corresponsing References & Context in the Transcript
           - Credibility: 
           - Reliability: 
           - Intimacy: 
           - Self-Orientation:    
        input_text: 
        {sp_text}
        """
    analyzer = ExclusionAnalyzer(args.input_dir, args.output_file)
    analyzer.analyze_v1(args.start_page, args.end_page, template, 2, 1)